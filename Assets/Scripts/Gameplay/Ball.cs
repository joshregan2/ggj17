﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	[SerializeField] float destructionHeight = -20;

	[SerializeField] float bounceForce = 10;

	Rigidbody2D body;

	private void Awake()
	{
		body = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update ()
	{
		if(transform.position.y < destructionHeight)
		{
			Destroy(gameObject);
			Entry.instance.DropBall();
		}
	}
	
	private void OnCollisionEnter2D(Collision2D collision)
	{
		body.velocity = (collision.contacts[0].normal * bounceForce);
	}
}
