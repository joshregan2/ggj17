﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialControl : MonoBehaviour, IDragHandler
{
	#region Fields and Properties

	public enum WaveProperty
	{
		Frequency,
		Amplitude,
		Shape
	};

	public float StepAmount;
	public float CurrentValue;

	public string Title;
	public WaveProperty LinkedWaveProperty;

	public Image DialImage;
	public Text ValueText;
	public Text TitleText;

	private WaveEmitter waveEmitter;

	#endregion

	#region Lifecycle

	protected void Awake()
	{
		StepAmount = 0.025f;
		CurrentValue = 0;
	}

	// Use this for initialization
	protected void Start()
	{
		TitleText.text = Title;
		UpdateDialRotation();
	}

	// Update is called once per frame
	protected void Update()
	{

	}

	public void ConnectToWaveEmitterWithTag(string waveEmitterTag)
	{
		waveEmitter = GameObject.FindGameObjectWithTag(waveEmitterTag).GetComponent<WaveEmitter>();
	}

	#endregion

	#region Handle Input

	public void OnBeginDrag(PointerEventData eventData)
	{

	}

	public void OnDrag(PointerEventData data)
	{
		Vector2 normalizedDelta = data.delta.normalized;
		//float magnitude = data.delta.magnitude;
		float change = 0;
		if (normalizedDelta.x > 0)
		{
			change = StepAmount;
		}
		else if (normalizedDelta.x < 0)
		{
			change -= StepAmount;
		}

		CurrentValue = Mathf.Clamp(CurrentValue + change, 0, 1.0f);

		int percentage = (int)(CurrentValue * 100.0f);
		ValueText.text = percentage.ToString();

		UpdateDialRotation();

		if (waveEmitter != null) 
		{
			switch (LinkedWaveProperty) 
			{
			case WaveProperty.Frequency:
				waveEmitter.frequencyInput = CurrentValue;
				break;
			case WaveProperty.Amplitude:
				waveEmitter.amplitudeInput = CurrentValue;
				break;
			case WaveProperty.Shape:

				break;
			}
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{

	}

	private void UpdateDialRotation()
	{
		float maxRotation = 112.0f;
		float minRotation = -112.5f;
		float rotationRange = maxRotation - minRotation;
		float newRotation = minRotation + (rotationRange * (1.0f - CurrentValue));

		DialImage.transform.localRotation = Quaternion.AngleAxis(newRotation, Vector3.forward);
	}

	#endregion
}

