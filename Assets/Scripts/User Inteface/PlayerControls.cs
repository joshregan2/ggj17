﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{
	#region Fields and Properties

	public string WaveEmitterTag;

	public DialControl AmplitudeDial;
	public DialControl FrequencyDial;
	public DialControl ShapeDial;

	#endregion

	#region Lifecycle

	protected void Awake()
	{

	}

	// Use this for initialization
	protected void Start()
	{
		if (WaveEmitterTag != null && WaveEmitterTag.Length > 0)
		{
			if (AmplitudeDial != null) 
			{
				AmplitudeDial.ConnectToWaveEmitterWithTag(WaveEmitterTag);
			}
			if (FrequencyDial != null) 
			{
				FrequencyDial.ConnectToWaveEmitterWithTag(WaveEmitterTag);
			}
			if (ShapeDial != null) 
			{
				ShapeDial.ConnectToWaveEmitterWithTag(WaveEmitterTag);
			}
		}
	}
	
	// Update is called once per frame
	protected void Update()
	{
	
	}

	#endregion
}

